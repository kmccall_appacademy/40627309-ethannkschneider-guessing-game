# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game
  secret_number = (1...100).to_a.shuffle[0]
  puts "The Computer has chosen a number."
  correct = false
  number_of_guesses = 0

  until correct
    puts "Guess a number: "
    user_guess = gets.chomp.to_i
    number_of_guesses += 1
    puts user_guess
    if user_guess == secret_number
      correct = true
    elsif user_guess > secret_number
      puts "too high"
    elsif user_guess < secret_number
      puts "too low"
    end
  end
  puts "Congratulations! You guessed the number in #{number_of_guesses} guesses!"
end

def file_shuffle
  puts "Which file would you like to shuffle? E.g. 'io_test'"
  input_name = gets.chomp
  contents = File.readlines("#{input_name}.txt")
  contents.shuffle!
  new_file = File.open("#{input_name}-shuffled.txt", "w")
  contents.each do |line|
    new_file.puts line
    $stdout.puts line
  end
  new_file.close
end

if __FILE__ == $PROGRAM_NAME
  guessing_game
  #file_shuffle
end
